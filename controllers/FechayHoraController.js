//   4.- Realizar el CRUD correspondiente a la relación de estudiante, docente, examen.
// El proceso se realizara en un array de json de estudiantes, docentes y exámenes
// Subir al repositorio de GitLab.
// 5.- Obtener la fecha y hora del servidor, con el siguiente formato de ejemplo:
// {
// “day”: “viernes”,
// “date_1”: “6 de Mayo”,
// “year”: 2022,
// “date_2”: 06/05/2022,
// “hour”: “10:30:12”,
// “prefix”:

const { request ,  response } = 'express';

const meses = ['Enero','Febrero','Marzo','Abril','Mayo','junio','Julio','Agosto', 'Septiembre','Octubre', 'Noviembre','Diciembre'];
const  dias = ['domingo','lunes','martes','miércoles','jueves','viernes','sábado'];


const cero = (num) => {
    console.log(num);
return num < 10 ? '0'+num : num;

}

const FechadelServidor = (req, res = response) => {
    const date = new Date();
    res.json({
       
        day  : ` ${dias[date.getDay()]} `,

        date_1 : `${date.getDate()} De ${meses[date.getMonth()]}`,

        year   : date.getFullYear(),
        
        date_2 : `${cero(date.getDate())}/${cero(date.getMonth()+1)}/${date.getFullYear()}`,

        hour   : `${cero(date.getHours())}:${cero(date.getMinutes())}:${cero(date.getSeconds())}`,
        
        prefix : `${date.getHours() > 11? 'PM': 'AM'}`
        
    })

};
module.exports = {
    FechadelServidor 
  };















